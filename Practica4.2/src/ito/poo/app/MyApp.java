package ito.poo.app;

import ito.poo.clases.Fruta;
import ito.poo.clases.Periodo;

public class MyApp {

	public static void main(String[] args) {
		
		Fruta fruta1;
		fruta1=new Fruta("Pi�a", 120f, 120.000f, 300.000f);
	    System.out.println(fruta1);
	    
	    Periodo periodo;
	    periodo=new Periodo("8 meses", 40.000f);
	    System.out.println(periodo);
	}
}
